import { Component, OnInit, Input } from '@angular/core';
 import {SharedService} from '../shared-service/shared.service'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() parentMessage:string;
  message:string;
  
  constructor(private sharedService :SharedService) { }

  ngOnInit(): void {
    this.sharedService.currentMessage.subscribe(message => this.message = message)
  }

}
