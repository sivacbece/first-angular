import { Injectable } from '@angular/core';
import { BehaviorSubject,Observable } from 'rxjs';
import { HttpClient,HttpResponse,HttpHeaders  } from '@angular/common/http';
import {config,employee,employees} from '../types/shared.type';

const configURL="assets/config.json";
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};
@Injectable({
  providedIn: 'root'
})
export class SharedService {
  
  
  private configuration:config;
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();

  constructor(private http: HttpClient ) {
    this.getConfig().subscribe(data => {
      this.configuration=data;
     });
   }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }

  getDataText():Observable<any>{
    return this.http.get<any>("assets/data.txt");
  }

  getEmployees():Observable<any>{
    return this.http.get<employees>( this.configuration.APIBaseURL+"employees", { observe: 'response' });
  }
  addEmployee(emp:employee):Observable<any>{
    return this.http.post<any>( this.configuration.APIBaseURL+"employees",emp,httpOptions);
  }
  updateEmployee(emp:employee):Observable<any>{
    return this.http.put<any>( this.configuration.APIBaseURL+"employees/"+emp.id,emp,httpOptions);
  }
  deleteEmployee(id:number):Observable<any>{
    return this.http.delete<any>( this.configuration.APIBaseURL+"employees/"+id,httpOptions);
  }
  getConfig(): Observable<config> {
    return this.http.get<config>(configURL);
  }
}
