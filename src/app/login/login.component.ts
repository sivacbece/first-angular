import { Component, OnInit } from '@angular/core';
import {SharedService} from '../shared-service/shared.service';
import {config} from '../types/shared.type';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  message:string;
  configuration:config;
  constructor(private sharedService :SharedService) { }

  ngOnInit(): void {
    this.sharedService.currentMessage.subscribe(message => this.message = message)
    this.sharedService.getConfig().subscribe(data => {
      console.log(data);
     this.configuration = data;
    }, err => {
      console.log(err);
    });

    this.getDataText();
  }
  changeSharedServiceValue(){
    this.sharedService.changeMessage("Hello from Sibling")
  }
  getDataText(){
    this.sharedService.getDataText().subscribe(result=>{
        console.log(result);
    },
    err=>{
      console.log(err);
    })
  }

}
