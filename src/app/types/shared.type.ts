export interface registration {
   name:string;
   username:string;
   email:string;
   age:number;
   phonenumber:string;
   address:string
}
export interface config{
   APIBaseURL:string;
   APIKey:string;
   employeeJSONURL:string;
}

export interface employee{
   id:number;
   employee_name:string;
   username:string;
   email_address:string;
   phone:string;
   gender:string;
   employee_address:string;
   city:string;
   created_date:string;
   modified_date:string;
   is_active:boolean;
}

export interface employees{
   employees:[employee];
}