import { Component, OnInit,Output ,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
footerComponentValue:string="footerComponentValue";
@Output() messageEvent = new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
  }
  updateMessage() {
    this.messageEvent.emit(this.footerComponentValue)
  }
}
