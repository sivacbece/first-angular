import { Component, OnInit } from '@angular/core';
import { employee, employees } from '../types/shared.type';
import { SharedService } from '../shared-service/shared.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  displayedColumns: string[] = ['id', 'employee_name', 'username', 'email_address', 'phone', 'gender', 'employee_address', 'city', 'is_active','edit','delete'];
  dataSource: Array<employee> = [];
  showEmployeeList: boolean = false;
  showEmployeeForm: boolean = true;
  frmEmployee: employee = {
    username: "",
    city: "",
    email_address: "",
    employee_address: "",
    employee_name: "",
    gender: "",
    id: 0,
    phone: "",
    is_active: true,
    created_date:null,
    modified_date:null
  };
  constructor(private sharedService: SharedService,private toastr: ToastrService) { }

  ngOnInit(): void {

  }
  loadAllEmployees() {
    this.showEmployeeList = true;
    this.showEmployeeForm = false;
    this.sharedService.getEmployees().subscribe(result => {
      console.log(result.body);
      this.dataSource = result.body;
    },
      err => {
        console.log(err);
      })
  }
  addNewEmployee() {
    this.showEmployeeList = false;
    this.showEmployeeForm = true;
    this.frmEmployee = {
      username: "",
      city: "",
      email_address: "",
      employee_address: "",
      employee_name: "",
      gender: "",
      id: 0,
      phone: "",
      is_active: true,
      created_date:null,
      modified_date:null
    };
  }
  clearEmployeeFrm(){
    this.frmEmployee = {
      username: "",
      city: "",
      email_address: "",
      employee_address: "",
      employee_name: "",
      gender: "",
      id: 0,
      phone: "",
      is_active: true,
      created_date:null,
      modified_date:null
    };
  }
  addEmployee(){
    this.sharedService.addEmployee(this.frmEmployee).subscribe(result => {
      this.toastr.success("Employee Added");
      this.loadAllEmployees();
    },
      err => {
        console.log(err);
      })
  }

  updateEmployee(){
    this.sharedService.updateEmployee(this.frmEmployee).subscribe(result => {
      this.toastr.success("Employee Updated");
      this.loadAllEmployees();
    },
      err => {
        console.log(err);
      })
  }
  editEmployee(emp:employee){
    this.frmEmployee=emp;
    this.showEmployeeList = false;
    this.showEmployeeForm = true;
  }
  deleteEmployee(emp:employee){

    var r = confirm("Are you sure want to delete!");
    if (r == true) {
      this.sharedService.deleteEmployee(emp.id).subscribe(result => {
        this.toastr.success("The employee deleted successfully");
        this.loadAllEmployees();
      },
        err => {
          console.log(err);
        })
    } else {
     
    }
  }
}
