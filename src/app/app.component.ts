import { Component,ViewChild } from '@angular/core';
import {FooterComponent} from '../app/footer/footer.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'first-angular-app';
  // message:string="Message from Parenet Component";
  @ViewChild(FooterComponent) footerComponent;

  // ngAfterViewInit() {
  //   this.message = this.footerComponent.footerComponentValue;
  // }
  // getValueFromFooter(){
  //   this.message = this.footerComponent.footerComponentValue;
  // }
  // receiveMessage(message){
  //   this.message =message;
  // }
}
