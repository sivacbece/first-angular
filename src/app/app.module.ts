import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from './material-module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
// import { ToasterModule } from 'ngx-toaster/src/lib';
import { ToastrModule } from 'ngx-toastr';
import { HighlightDirective } from './directive/highlight.directive';
import { DefaultimagePipe } from './pipes/defaultimage.pipe';
import {SharedService} from '../app/shared-service/shared.service';
import { HttpClientModule } from '@angular/common/http';
import { EmployeeComponent } from './employee/employee.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    LoginComponent,
    RegistrationComponent,
    HighlightDirective,
    DefaultimagePipe,
    EmployeeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({timeOut:5000,positionClass:"toast-bottom-right",preventDuplicates:true}),
    HttpClientModule
  ],
  providers: [SharedService],
  entryComponents:[SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
